TEMPLATE = app
CONFIG += console c++11
CONFIG -= app_bundle
CONFIG -= qt

LIBS += -lgxiapi \
        -lpthread \
        -lX11

#添加头文件路径
INCLUDEPATH += /usr/include/opencv4

#添加需要链接的库
LIBS += -L/usr/lib/aarch64-linux-gnu -lopencv_core -lopencv_imgcodecs -lopencv_imgproc -lopencv_highgui -lopencv_objdetect -lopencv_ml -lopencv_video -lopencv_videoio  -lopencv_calib3d -lopencv_dnn -lopencv_features2d -lopencv_flann -lopencv_gapi -lopencv_photo -lopencv_stitching

SOURCES += main.cpp \
    GxCamera/GxCamera.cpp

HEADERS += \
    GxCamera/include/DxImageProc.h \
    GxCamera/include/GxIAPI.h \
    GxCamera/GxCamera.h
